function read_more_toggle(clicked_elem, hidden_text_elem_id) {
  var hidden_text = document.getElementById(hidden_text_elem_id);
  
  hidden_text.classList.toggle("expanded");
  hidden_text.classList.toggle("contracted");
  hidden_text.parentElement.parentElement.classList.toggle("cardback-expanded");

  if (hidden_text.classList.contains("expanded")) {
    clicked_elem.innerHTML = "read less...";
  } else {
    clicked_elem.innerHTML = "read more...";
  }
}
